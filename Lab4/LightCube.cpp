#include "LightCube.h"






LightCube::LightCube(GLfloat * lightPosition)
{
	this->lightPosition = lightPosition;
}

void LightCube::draw()
{
	float x = lightPosition[0];
	float y = lightPosition[1];
	float z = lightPosition[2];
	glTranslatef(x, y, z);
	glutSolidCube(0.2);
	GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 100.0);

	glEnable(GL_LIGHT0);
	glTranslatef(-x, -y, -z);

}

LightCube::~LightCube()
{
}
