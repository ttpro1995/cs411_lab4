#pragma once
#include <GL/glut.h>
class LightCube
{
private:
	GLfloat* lightPosition;
public:
	LightCube(GLfloat* lightPosition);
	void draw();
	~LightCube();
};

