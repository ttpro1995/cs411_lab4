#pragma once
#include <GL/glut.h>
#include <stdlib.h>
#include <GL\GL.h>
#include <png.h>
GLuint LoadTexture(const char * filename);
GLuint png_texture_load(const char * file_name, int * width, int * height);