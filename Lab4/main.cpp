#include <GL/glut.h>
#include "LightCube.h"
#include <stdlib.h>
#include"helper.h"
#include <iostream>

GLfloat rtri; // Angle For The Triangle ( NEW )

GLfloat rquad; // Angle For The Quad ( NEW )

// texture
#define NUM_TEXTURES 3
GLuint texture[NUM_TEXTURES];
char* texNames[NUM_TEXTURES] = { "tex/brick.PNG", "tex/earth.PNG", "tex/fire.PNG" };
//GLuint i = 0;

// light
bool light = FALSE;
GLfloat lightAmbient[] = { 0.5f,0.5f,0.5f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPositionA[] = { 0.0f,0.0f,2.0f,1.0f};
//GLfloat lightPositionB[] = { 1.0f,0.0f,2.0f,1.0f };
//GLfloat lightPositionC[] = { 1.0f,1.0f,2.0f,1.0f };
//GLfloat lightPositionD[] = { 0.0f,1.0f,2.0f,1.0f };
LightCube* lightCube;

//blend
bool blend = FALSE; // blending on/off

void specialKeyboardInput(int key, int x, int y);

float const MOVE_LIGHT_UNIT = 0.1f;

void loadAllTexture() {
	int* width = new int(80);
	int* height = new int(80);
	for (int i = 0; i < NUM_TEXTURES; i++) {
		texture[i] = png_texture_load(texNames[i],width,height);
		std::cout <<texture[i] <<" \n";
	}

}


void init(void)

{
	loadAllTexture();
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH); // Enable Smooth Shading (7)
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background (8)

	glClearDepth(1.0f); // Depth Buffer Setup

	glEnable(GL_DEPTH_TEST); // Enables Depth Testing

	glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective Calculations

	//glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPositionA);


	glEnable(GL_LIGHT0);
	//glEnable(GL_LIGHT2);
	//glEnable(GL_LIGHT3);
	//glEnable(GL_LIGHT4);
	lightCube = new LightCube(lightPositionA);


	glColor4f(1.0f, 1.0f, 1.0f, 0.5);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
}

void displayPyramid(void)

{

	glLoadIdentity(); // Reset The Current Modelview Matrix (1)
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glTranslatef(-1.5f, 1.0f, -6.0f); // Move Left 1.5 Units And Into The Screen 6.0 (2)

	glRotatef(rtri, 0.0f, 1.0f, 0.0f); // Rotate The Triangle On The Y axis ( NEW )(3)

	glBegin(GL_TRIANGLES); // Start Drawing A Triangle (4)

	//glColor3f(1.0f, 0.0f, 0.0f); // Red (5)
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // Top Of Triangle (Front) (6)
	//glColor3f(0.0f, 1.0f, 0.0f); // Green
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // Left Of Triangle (Front)
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // Right Of Triangle (Front)

	//glColor3f(1.0f, 0.0f, 0.0f); // Red
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // Top Of Triangle (Right)
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // Left Of Triangle (Right)
	//glColor3f(0.0f, 1.0f, 0.0f); // Green
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // Right Of Triangle (Right)

	//glColor3f(1.0f, 0.0f, 0.0f); // Red
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // Top Of Triangle (Back)
	//glColor3f(0.0f, 1.0f, 0.0f); // Green
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // Left Of Triangle (Back)
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // Right Of Triangle (Back)

	//glColor3f(1.0f, 0.0f, 0.0f); // Red
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // Top Of Triangle (Left)
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // Left Of Triangle (Left)
	//glColor3f(0.0f, 1.0f, 0.0f); // Green
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // Right Of Triangle (Left)

	glEnd(); //(4) // Done Drawing The Pyramid
}

void displayCube() {

	glLoadIdentity(); // Reset The Current Modelview Matrix
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glTranslatef(1.5f, 1.0f, -7.0f); // Move Right 1.5 Units And Into The Screen 7.0

	glRotatef(rquad, 1.0f, 1.0f, 1.0f); // Rotate The Quad On The X axis ( NEW )




	glBegin(GL_QUADS); // Draw A Quad

	//TOP
	//glColor3f(0.0f, 1.0f, 0.0f); // Set The Color To Green
	glColor3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Quad (Top)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Quad (Top)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, 1.0f, 1.0f); // Bottom Left Of The Quad (Top)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 1.0f, 1.0f); // Bottom Right Of The Quad (Top)


	//Color3f(1.0f, 0.5f, 0.0f); // Set The Color To Orange

	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, -1.0f, 1.0f); // Top Right Of The Quad (Bottom)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, -1.0f, 1.0f); // Top Left Of The Quad (Bottom)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Left Of The Quad (Bottom)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Right Of The Quad (Bottom)

	//Color3f(1.0f, 0.0f, 0.0f); // Set The Color To Red

	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, 1.0f); // Top Right Of The Quad (Front)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, 1.0f, 1.0f); // Top Left Of The Quad (Front)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Left Of The Quad (Front)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Right Of The Quad (Front)

	//glColor3f(1.0f, 1.0f, 0.0f); // Set The Color To Yellow

	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, -1.0f, -1.0f); // Top Right Of The Quad (Back)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, -1.0f, -1.0f); // Top Left Of The Quad (Back)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, 1.0f, -1.0f); // Bottom Left Of The Quad (Back)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 1.0f, -1.0f); // Bottom Right Of The Quad (Back)

	//glColor3f(1.0f, 1.0f, 1.0f); // Set The Color To Blue

	glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f, 1.0f, 1.0f); // Top Right Of The Quad (Left)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f, 1.0f, -1.0f); // Top Left Of The Quad (Left)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f); // Bottom Left Of The Quad (Left)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f, 1.0f); // Bottom Right Of The Quad (Left)

	//glColor3f(1.0f, 0.0f, 1.0f); // Set The Color To Violet

	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 1.0f, -1.0f); // Top Right Of The Quad (Right)
	glTexCoord2f(0.0f, 1.0f); glVertex3f(1.0f, 1.0f, 1.0f); // Top Left Of The Quad (Right)
	glTexCoord2f(0.0f, 0.0f); glVertex3f(1.0f, -1.0f, 1.0f); // Bottom Left Of The Quad (Right)
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, -1.0f, -1.0f); // Bottom Right Of The Quad (Right)

	glEnd();

	// Done Drawing The Quad
}

void displaySphere() {
	glLoadIdentity(); // reset the current modelview matrix
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glTranslatef(-0.5f, 2.5f, -7.0f);
	glRotatef(-rquad, 1.0f, 1.0f, 1.0f);
	GLUquadricObj *q;
	q = gluNewQuadric();
	gluQuadricDrawStyle(q, GLU_FILL);
	gluQuadricNormals(q, GLU_SMOOTH);
	gluQuadricTexture(q, GL_TRUE);
	gluSphere(q, 1.0, 100, 100);
	gluDeleteQuadric(q);
}

void displayCylinder() {
	glLoadIdentity(); // reset the current modelview matrix
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTranslatef(2.0f, -0.5f, -5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f); // make the cylender stand up
	glRotatef(rtri, 0.0f, 0.0f, 1.0f); // Rotate The cylinder On The z axis 
	GLUquadricObj* q;
	q = gluNewQuadric();
	gluQuadricDrawStyle(q, GLU_FILL);
	gluQuadricNormals(q, GLU_SMOOTH);
	gluQuadricTexture(q, GL_TRUE);
	GLdouble top_radius = 0.5;
	GLdouble bottom_radius = 0.5;
	GLdouble slice = 100;
	GLdouble stack = 100;
	GLdouble height = 2;
	gluDisk(q, 0.0f, top_radius, slice, 1);
	gluCylinder(q, bottom_radius, top_radius, height, slice, stack);
	glTranslatef(0.0f,0.0f,height);
	gluDisk(q, 0.0f, top_radius, slice, 1);
	gluDeleteQuadric(q);
}

void displayCone() {
	
		glLoadIdentity(); // reset the current modelview matrix
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTranslatef(-2.0f, -0.5f, -5.0f);
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f); // make the cylender stand up
		glRotatef(rtri, 0.0f, 0.0f, 1.0f); // Rotate The cylinder On The z axis 
		GLUquadricObj* q;
		q = gluNewQuadric();
		gluQuadricDrawStyle(q, GLU_FILL);
		gluQuadricNormals(q, GLU_SMOOTH);
		gluQuadricTexture(q, GL_TRUE);
		GLdouble top_radius = 0.5;
		GLdouble bottom_radius = 0;
		GLdouble slice = 100;
		GLdouble stack = 100;
		GLdouble height = 2;
		gluDisk(q, 0.0f, bottom_radius, slice, 1);
		gluCylinder(q, bottom_radius, top_radius, height, slice, stack);
		//glTranslatef(0.0f, 0.0f, height);
		gluDisk(q, 0.0f, bottom_radius, slice, 1);
		gluDeleteQuadric(q);
}

void displaySolidTorus() {
	glLoadIdentity();
	glTranslatef(0.0f, -2.0f, -10.0f);
	glRotatef(rquad, 0.0f, 1.0f, 0.0f);
	//glTranslatef(4.0f, 0.0f, -14.0f);
	glutWireTorus(0.5, 1, 20, 50);
}



void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear Screen And Depth Buffer

	glPushMatrix(); //(9)

	displayPyramid();
	displayCube();
	displaySphere();
	displayCylinder();
	displayCone();
	displaySolidTorus();
	lightCube->draw();
	//rtri += 0.05f;
	//rquad -= 0.025f;

	glPopMatrix();//(10)
	glutSwapBuffers();//(11)
	glutPostRedisplay();
}
void reshape(int w, int h) {

	glViewport(0, 0, (GLsizei)w, (GLsizei)h);

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}
	void keyboard(unsigned char key, int x, int y)
	{

		switch (key) {
		
		case 'w':
			// move light cube in the screen
			lightPositionA[2] -= MOVE_LIGHT_UNIT;
			break;

		case 's':
			//move light out of the screen
			lightPositionA[2] += MOVE_LIGHT_UNIT;
			break;

		case 'p':
			rtri += 0.2f;
			glutPostRedisplay(); //(12)
			break;

		case 'c':
			rquad -= 0.15f;
			glutPostRedisplay();
			break;

		case 'l':
			light = !light;
			if (!light)
			{
				glDisable(GL_LIGHTING);
			}
			else {
				glEnable(GL_LIGHTING);
			}
			break;

		case 'b':
			blend = !blend;
			if (blend) {
				glEnable(GL_BLEND);
				glDisable(GL_DEPTH_TEST);
			}
			else {
				glDisable(GL_BLEND);
				glEnable(GL_DEPTH_TEST);	
			}
			break;

		case 27: //ESC

			exit(0);

			break;

		default:

			break;

		}

	}

	int main(int argc, char** argv)

	{

		glutInit(&argc, argv);

		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

		glutInitWindowSize(500, 500);

		glutInitWindowPosition(100, 100);

		glutCreateWindow(argv[0]);

		init();

		glutDisplayFunc(display); //(13)

		glutReshapeFunc(reshape); //(14)

		glutKeyboardFunc(keyboard);//(15)
		glutSpecialFunc(specialKeyboardInput);

		glutMainLoop();

		return 0;

	}

	void specialKeyboardInput(int key, int x, int y) {
		switch (key) {
		case GLUT_KEY_UP:
			std::cout << "UP \n";
			lightPositionA[1] += MOVE_LIGHT_UNIT;
			//lightPositionB[1] += 1;
			//lightPositionC[1] += 1;
			//lightPositionD[1] += 1;
			glutPostRedisplay();
			break;
		case GLUT_KEY_DOWN:
			std::cout << "DOWN \n";

			lightPositionA[1] -= MOVE_LIGHT_UNIT;
			//lightPositionB[1] -= 1;
			//lightPositionC[1] -= 1;
			//lightPositionD[1] -= 1;
			glutPostRedisplay();
			break;
		case GLUT_KEY_LEFT:
			std::cout << "LEFT \n";
			lightPositionA[0] -= MOVE_LIGHT_UNIT;
			//lightPositionB[0] -= 1;
			//lightPositionC[0] -= 1;
			//lightPositionD[0] -= 1;
			glutPostRedisplay();
			break;
		case GLUT_KEY_RIGHT:
			std::cout << "RIGHT \n";
			lightPositionA[0] += MOVE_LIGHT_UNIT;
			//lightPositionB[0] += 1;
			//lightPositionC[0] += 1;
			//lightPositionD[0] += 1;
			glutPostRedisplay();
			break;
		default:

			break;

		}
	}